package com.sourceit.firstandroidproject;

/**
 * Created by wenceslaus on 13.01.18.
 */

public interface ActivityCommunication {
    void updateText(String str);
}
